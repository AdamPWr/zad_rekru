#include"Shop.hpp"
#include"Common.hpp"
#include<string>
#include<iostream>
#include<algorithm>

Car Shop::getCarInfoFromSeller() noexcept
{
    Car car;
    std::cout<<"Model name:"<<std::endl;
    std::cin>>car.model;
    std::cout<<"Year:"<<std::endl;
    std::cin>>car.year;
    std::cout<<"Price"<<std::endl;
    std::cin>>car.price;
    return car;
}

void Shop::listAvailableCars() noexcept
{
    std::lock_guard<std::mutex> lock(mutex); 
    std::for_each(carsInShop.cbegin(),
        carsInShop.cend(),
        [](const auto& id_car_pair){std::cout<<"Car ID:"<<id_car_pair.first<<std::endl; id_car_pair.second.displayCarInfo();});
}

void Shop::sellCar()
{
    size_t carID = carsInShop.size();
    {
        std::lock_guard<std::mutex> lock(mutex);
        carsInShop.insert(std::make_pair<size_t,Car>(static_cast<size_t&&>(carID),getCarInfoFromSeller()));
    }
    

    auto decreaseCarValueFunc = [carID](std::map<short,Car>& cars, float transactionPrice, std::mutex& mutex){

        std::this_thread::sleep_for(std::chrono::seconds(30));

        float currentCarPrice;
        std::unique_lock lock{mutex, std::defer_lock};
        do
        {
            lock.lock();
            auto car = cars.find(carID);
            if(car == cars.end()) return;
            car->second.price*=0.999;
            currentCarPrice = car->second.price;
            lock.unlock();
            std::this_thread::sleep_for(std::chrono::seconds(1));
        } while (transactionPrice * 0.8 <= currentCarPrice);
        
    };
    try
    {
        std::thread t(decreaseCarValueFunc,std::ref(carsInShop),carsInShop.at(carID).price,std::ref(mutex));
        t.detach();
    }
    catch(const std::out_of_range& e)
    {
        std::cerr << e.what() << std::endl;
        std::cout<<"Price devaluation task did not start"<<std::endl;
    }
    
    std::cout<<"Car sold"<<std::endl;
}

void Shop::buyCar()
{
    listAvailableCars();
    std::cout<<"Select car ID"<<std::endl;
    short id;
    std::cin>>id;
    auto iter = carsInShop.find(id);
    if(iter == carsInShop.end())
    {
        std::cout<<"Wrong car id"<<std::endl;
        return;
    }
    else{
        std::lock_guard<std::mutex> lock(mutex); 
        soldCars.insert(iter->second);
        carsInShop.erase(iter);
        std::cout<<"Car sold"<<std::endl;
    }
}

std::multiset<Car> Shop::getSoldCars() noexcept
{
    return soldCars;
}

void Shop::clearSoldThisDay() noexcept
{
    std::lock_guard<std::mutex> lock(mutex); 
    soldCars.clear();
}