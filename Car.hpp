#pragma once
#include<string>

struct Car
{
    std::string model;
    float price;
    unsigned short year;

     bool operator<(const Car& other) const {
        return price < other.price;
    }

    void displayCarInfo() const;
};