# Nazwa programu
TARGET = shop_app

# Kompilator C++
CXX = g++

# Flagi kompilatora
CXXFLAGS = -std=c++17 -Wall -Wextra -pthread

# Pliki źródłowe
SRCS =  main.cpp Shop.cpp Car.cpp Common.cpp

# Pliki obiektowe
OBJS = $(SRCS:.cpp=.o)

# Zależności i reguły
$(TARGET): $(OBJS)
	$(CXX) $(CXXFLAGS) -o $@ $^

# Reguła kompilacji plików źródłowych do plików obiektowych
%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

# Czyszczenie plików tymczasowych i wynikowych
clean:
	rm -f $(OBJS) $(TARGET)
