#include<iostream>
#include<memory>
#include<thread>
#include"Shop.hpp"
#include"Common.hpp"


int main()
{
    std::unique_ptr<IShop> shop_ptr = std::make_unique<Shop>();
    short choice;
    while(choice !=4)
    {
        std::cout<<MENU;
        std::cin>>choice;

        switch (choice)
        {
            case 1:
            {
                shop_ptr->buyCar();
                break;
            }
            case 2:
            {
                shop_ptr->sellCar();
                break;
            }
            case 3:
            {
                for(const auto& car : shop_ptr->getSoldCars())
                {
                    std::cout<<"______________________"<<std::endl;
                    car.displayCarInfo();
                }
                std::this_thread::sleep_for(std::chrono::seconds(5));
            }
            case 4:
            {
                break;
            }
            default:
            {
                std::cout<<"Choosed wrong number"<<std::endl;
                break;
            }           
        }
        clear_screen();
    }
    return 0;
}