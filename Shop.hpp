#pragma once
#include<map>
#include<set>
#include<thread>
#include <mutex>
#include"Car.hpp"


class IShop
{
public:
    virtual void sellCar() = 0;
    virtual void buyCar() = 0;
    virtual std::multiset<Car> getSoldCars() = 0;
    virtual void clearSoldThisDay() = 0;
};

class Shop : public  IShop
{

    std::map<short,Car> carsInShop;
    std::multiset<Car> soldCars;
    std::mutex mutex;

    Car getCarInfoFromSeller() noexcept;
    void listAvailableCars() noexcept;
public:
    virtual void sellCar() override;
    virtual void buyCar() override;
    virtual std::multiset<Car> getSoldCars() noexcept override;
    virtual void clearSoldThisDay() noexcept override;

};
